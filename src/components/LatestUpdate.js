import React from 'react'
import List from './List'

const LatestUpdate = () => {
  return (
    <div><h1 className='p-2 m-2 text-3xl font-light '>Latest Update</h1>
   <List Heading="Malware Attacks"></List>
   <List Heading="Application repacked with Trojan horses"></List>
   <List Heading="Remote Mining Rig"></List>
   <List  Heading="Fake call center scams"></List>
   <List   Heading="New Cybersecurity Tips shared by the community"></List>

   </div>
  )
}

export default LatestUpdate
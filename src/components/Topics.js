import React from 'react';
import Link from 'next/link';

const Topics = () => {
  // Example data for the table, which can be replaced or extended.
  const topicsData = [
    { guide: "Phishing 101", story: "Johns Mistake", forum: "Ask Questions", achievement: true },
    { guide: "Secure Passwords", story: "The Hacker Attack", forum: "Share Experiences", achievement: false },
    { guide: "Public Wi-Fi", story: "The Coffee Shop Incident", forum: "Discuss Tips", achievement: true },
    { guide: "Social Engineering", story: "The Phone Call", forum: "Share Stories", achievement: false },
    { guide: "Malware Protection", story: "The Infected File", forum: "Ask for Help", achievement: true },
    { guide: "Catfishing", story: "The Stolen Laptop", forum: "Share Advice", achievement: false },
  ];

  return (
    <div className='w-full lg:w-[690px] p-4 grow'>
      <h1 className='text-3xl font-normal'>Explore Security Topics</h1>
      <div className='overflow-hidden w-full'>
      <div className="overflow-x-scroll">
  <table className='w-full p-2 m-2 mt-4 text-left whitespace-nowrap min-w-full'>
    <thead>
      <tr>
        <th>Guide</th>
        <th>Story</th>
        <th>Forum</th>
        <th>Achievement</th>
      </tr>
    </thead>
    <tbody>
      {topicsData.map((topic, index) => (
        <tr key={index} className="p-2 m-2">
          <td><Link href={topic.guide}>{topic.guide}</Link></td>
          <td><Link href={topic.story}>{topic.story}</Link></td>
          <td><Link href={topic.forum}>{topic.forum}</Link></td>
          <td>{topic.achievement ? '✅' : '❌'}</td>
        </tr>
      ))}
    </tbody>
  </table>
</div>
</div>

    </div>
  );
};

export default Topics;

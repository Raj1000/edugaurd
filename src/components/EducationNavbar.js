import React from "react";
import Link from "next/link";

const Navbar = () => {
  return (
      <header className="flex w-full justify-between px-10 lg:px-24 py-2 lg:py-8  dark:bg-slate-800 dark:text-white bg-gradient-to-r  from-40% from-slate-800 via-slate-700 to-60% to-slate-800 bg-opacity-50">
        <Link className="font-concert-one text-xl lg:text-2xl" href="/">EduGaurd</Link>
        <ul className="hidden lg:flex space-x-5 text-white dark:text-slate-401 ">
          <li className="">
            <Link href="/">Home</Link>
          </li>
          <li className="">
            {" "}
            <Link href="/InteractiveGuide">Interactive Guide</Link>
          </li>
          <li className="">
            <Link href="/CyberStories">Cyber Stories</Link>
          </li>
           <li className="">
            <Link href="/Community">Community</Link>
          </li>
          {/* <li className="">Bitcoin Wallets</li> */}
          <li className=""><Link href="/Education">Education</Link></li>
        </ul>
    </header>
  );
};

export default Navbar;

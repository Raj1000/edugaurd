import React from "react";
import Link from "next/link";

const List = ({Heading}) => {
  return (
    <div className="flex p-2 m-2">
      <Link href="/" className="border-2 rounded-full shrink-0  w-14 h-14 flex items-center justify-center" >
        <button>➕</button>
      </Link>
      <p className="p-2 m-2 font-normal">
       {Heading}
      </p>
    </div>
  );
};

export default List;

import React from 'react'
import Navbar from '@/components/EducationNavbar'
import Footer from '@/components/Footer/Footer'

const page = () => {
  return (
    <>
    <div className='bg-darkbg min-h-screen text-white'>
      <Navbar></Navbar>
      <h1 className='mt-10 font-concert-one text-3xl text-center'> Coming Soon!</h1>
    </div>
    <Footer></Footer>
    </>
  )
}

export default page
